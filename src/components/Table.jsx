import React, { Component } from "react";
import Swal from "sweetalert2";

export default class Table extends Component {
  constructor(props) {
      super(props);
      console.log(props.data);
  }

  btAlert = (ex) => {
    // console.log(ex);
    Swal.fire({
      title: `Stu_ID: ${ex.id}` +
        "</br>" + `Stu_Email: ${ex.stu_email}` +
        "</br>" + `Username: ${ex.stu_name==""?"Null":ex.stu_name
        }` + "</br>"
        + `Age: ${ex.stu_age}`,
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    })
  }

  render() {
    return (
      <div>
        <div class=" p-[100px]">
          <table class="w-full text-white ">
            <thead class=" bg-gray-50  dark:text-black ">
              <tr>
                <th scope="col" class="d1">
                  ID
                </th>
                <th scope="col" class="">
                  Email
                </th>
                <th scope="col" class="">
                  Username
                </th>
                <th scope="col" class="">
                  Age
                </th>
                <th scope="col" class="">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.data.map((e) => (
              
                <tr class={e.id%2===0?"tr2":"tr3"}>
                  <td>{e.id}</td>
                  <td>
                    {e.stu_email === "" ? (e.stu_email = "Null") : e.stu_email}
                  </td>
                  <td>{e.stu_name === "" ? (e.name = "Null") : e.stu_name}</td>
                  <td>{e.stu_age === "" ? (e.stu_age = "Null") : e.stu_age}</td>
                  <td class="p-[30px] d2">
                    <button class={e.status==="Pending"?'bt3':'bt1'} onClick={()=>{this.props.data2(e)}}>
                      {e.status}
                    </button>
                    <button onClick={() => { this.btAlert(e) }} class="bt2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                      Show more
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
