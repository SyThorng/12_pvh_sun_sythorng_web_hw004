import React, { Component } from "react";

export default class Title extends Component {
  render() {
    return (
      <div class="m">
        <h1 class="text-7xl font-bold text-purple-500 ml-[250px] mt-[40px]">
          {" "}
          Please Fill your Information
        </h1>
      </div>
    );
  }
}
