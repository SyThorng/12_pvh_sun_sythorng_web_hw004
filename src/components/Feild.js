import React, { Component } from "react";
import Table from "./Table";

export default class Feild extends Component {
  constructor() {
    super();
    this.state = {
      Student: [],
      newEmail: "",
      newName: "",
      newAge: "",
    };
  }
  txtName = (event) => {
    this.setState({
      newName: event.target.value,
    });
  };

  txtEmail = (event) => {
    this.setState({
      newEmail: event.target.value,
    });
  };

  txtAge = (event) => {
    this.setState({
      newAge: event.target.value,
    });
  };

  btnSubmit = () => {
    const objNew = {
      id: this.state.Student.length + 1,
      stu_email: this.state.newEmail,
      stu_name: this.state.newName,
      stu_age: this.state.newAge,
      status: "Pending"
    };

    this.setState({ Student: [...this.state.Student, objNew] });

    // this.setState({ Student: [...this.state.Student, objNew] }, () => {
    //   console.log(this.state.Student);
    // });
  };

  changeStatus = (event) => {
    event.status === "Pending" ? event.status = "Done" : event.status = "Pending";
    this.setState({ Student: [...this.state.Student] });
    // console.log(event);
  }
  render() {
    return (
      <div class="mt-[40px] ">
        <div class="p-[100px]">
          <label
            for="input-group-1"
            class="block mb-2 text-sm font-medium text-white"
          >
            Your Email
          </label>
          <div class="relative mb-6">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none ">
              <i class="fa-regular fa-envelope dark:bg-gray-600 font-bold"></i>
            </div>
            <input
              onChange={this.txtEmail}
              type="text"
              id="input-group-1"
              class=" bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="name@gmail.com"
            />
          </div>
          <label
            for="website-admin"
            class="mt-[30px] block mb-2 text-sm font-medium text-white"
          >
            Username
          </label>
          <div class="flex">
            <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              @
            </span>
            <input
              onChange={this.txtName}
              type="text"
              id="website-admin"
              class=" rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="@Username"
            />
          </div>
          <label
            for="website-admin"
            class="mt-[30px] block mb-2 text-sm font-medium text-white dark:text-white"
          >
            Age
          </label>
          <div class="flex">
            <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              ❤️
            </span>
            <input
              onChange={this.txtAge}
              type="text"
              id="website-admin"
              class="rounded-none rounded-r-lg bg-gray-50 border text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Age"
            />
          </div>
        </div>
        <button class="btn w-[300px] ml-[550px]" onClick={this.btnSubmit}>
          Register
        </button>
        <Table data={this.state.Student} data2={this.changeStatus} />
      </div>
    );
  }
}
