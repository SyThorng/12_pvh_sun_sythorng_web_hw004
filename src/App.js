import "./App.css";
import Feild from "./components/Feild";
import Register from "./components/Register";
import Table from "./components/Table";
import Title from "./components/Title";

function App() {
  return (
    <div className="App">
      <Title />
      <Feild />
      {/* <Table /> */}
    </div>
  );
}

export default App;
